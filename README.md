# segment_myxo_confocal_csgA

Code for segmentation and analysis of confocal images of Myxococcus xanthus mound formation.

Citation:
Short-Range C-Signaling Restricts Cheating Behavior during Myxococcus xanthus Development
Y Hoang, Joshua L. Franklin, Yann S. Dufour, Lee Kroos
